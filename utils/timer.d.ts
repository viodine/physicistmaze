export declare class Timer {
    private counter;
    private isRunning;
    private interval;
    constructor(counter?: number, isRunning?: boolean, interval?: number);
    readonly time: number;
    penalty(): void;
    start(): void;
    stop(): void;
}
