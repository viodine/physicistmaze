export class Timer {
    constructor(private counter: number = 0, private isRunning: boolean = false, private interval: number = 0) {}

    get time() {
        return this.counter;
    }

    penalty() {
        this.counter += 20;
    }

    start() {
        this.isRunning = true;
        this.interval = setInterval(() => {
            this.counter += 1;
        }, 1000)
    }

    stop() {
        this.isRunning = false;
        clearInterval(this.interval);
    }
}