import { Scene } from '@babylonjs/core';
export declare class Arrow {
    private scene;
    private answer;
    constructor(scene: Scene, answer: string);
    private corners;
    build(x: number, y: number, z: number, material: any): import("@babylonjs/core").Mesh;
}
