import { StandardMaterial, Color3 } from "@babylonjs/core";
import { mainScene } from "..";

export const trueMaterial = new StandardMaterial("trueMaterial", mainScene)
trueMaterial.diffuseColor = new Color3(0, 1, 0);

export const falseMaterial = new StandardMaterial("falseMaterial", mainScene)
falseMaterial.diffuseColor = new Color3(1, 0, 0);