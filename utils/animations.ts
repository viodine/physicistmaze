import {Animation, Vector3} from '@babylonjs/core';
import { FRAME_RATE } from '../index';
import * as event from './events'

export const introMoveAnimation = new Animation("cameraMoveIntro", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const introMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-608.773, 501.862, -271.969)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-1596.831, 1197.227, -477.010)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(31.290, -25.912, 130.072)
    }
]
introMoveAnimation.setKeys(introMoveKeyFrames);

export const OneToTwoMoveAnimation = new Animation("cameraMoveOneToTwo", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const OneToTwoMoveKeyframes = [
    {
        frame: 0,
        value: new Vector3(31.290, -25.912, 130.072)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(32.320, -34.350, 206.843)
    },
    {
        frame: 7*FRAME_RATE,
        value: new Vector3(97.524, -35.757, 215.244)
    },
    {
        frame: 11*FRAME_RATE,
        value: new Vector3(90.617, -37.035, 283.881)
    },
    {
        frame: 17*FRAME_RATE,
        value: new Vector3(-18.758, -40.129, 276.755)
    },
    {
        frame: 21*FRAME_RATE,
        value: new Vector3(-28.429, -35.488, 210.405)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-83.483, -36.428, 213.081)
    },
    {
        frame: 28*FRAME_RATE,
        value: new Vector3(-74.645, -36.533, 335.739)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-120.230, -36.267, 327.583)
    }
]
OneToTwoMoveAnimation.setKeys(OneToTwoMoveKeyframes);
OneToTwoMoveAnimation.addEvent(event.startJourney);
OneToTwoMoveAnimation.addEvent(event.firstQuestion);

export const TwoToThreeMoveAnimation = new Animation("cameraMoveTwoToThree", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const TwoToThreeMoveKeyframes = [
    {
        frame: 0,
        value: new Vector3(-120.230, -36.267, 327.583)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-146.011, -38.458, 392.954)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-87.134, -32.277, 396.459)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-86.528, -34.796, 450.994)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-209.646, -37.601, 447.508)
    },
    {
        frame: 23*FRAME_RATE,
        value: new Vector3(-206.354, -37.555, 512.357)
    },
    {
        frame: 26*FRAME_RATE,
        value: new Vector3(-266.582, -39.125, 513.356)
    },
    {
        frame: 29*FRAME_RATE,
        value: new Vector3(-261.241, -39.294, 628.243)
    },
    {
        frame: 32*FRAME_RATE,
        value: new Vector3(-324.656, -39.657, 625.043)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-321.081, -34.796, 863.554)
    },
    {
        frame: 38*FRAME_RATE,
        value: new Vector3(-383.495, -37.601, 860.556)
    },
    {
        frame: 42*FRAME_RATE,
        value: new Vector3(-383.827, -37.555, 801.030)
    },
    {
        frame: 46*FRAME_RATE,
        value: new Vector3(-444.331, -39.125, 807.686)
    },
    {
        frame: 50*FRAME_RATE,
        value: new Vector3(-443.620, -39.294, 867.788)
    },
    {
        frame: 53*FRAME_RATE,
        value: new Vector3(-510.896, -39.657, 858.734)
    },
    {
        frame: 56*FRAME_RATE,
        value: new Vector3(-495.845, -39.125, 989.197)
    },
    {
        frame: 60*FRAME_RATE,
        value: new Vector3(-613.506, -39.294, 985.387)
    },
    {
        frame: 64*FRAME_RATE,
        value: new Vector3(-614.460, -39.657, 929.652)
    },
    {
        frame: 67*FRAME_RATE,
        value: new Vector3(-555.460, -39.657, 930.652)
    },
    {
        frame: 70*FRAME_RATE,
        value: new Vector3(-560.460, -39.657, 809.652)
    },
    {
        frame: 73*FRAME_RATE,
        value: new Vector3(-617.460, -39.657, 817.652)
    },
    {
        frame: 75*FRAME_RATE,
        value: new Vector3(-617.460, -39.657, 684.652)
    },
    {
        frame: 77*FRAME_RATE,
        value: new Vector3(-686.460, -39.657, 698.652)
    }
]
TwoToThreeMoveAnimation.setKeys(TwoToThreeMoveKeyframes);
TwoToThreeMoveAnimation.addEvent(event.secondQuestion);

export const ThreeToFourMoveAnimation = new Animation("cameraMoveThreeToFor", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const ThreeToFourMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-686.460, -39.657, 698.652)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-674.174, -39.657, 749.742)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-742.219, -39.055, 742.184)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-737.797, -39.657, 665.886)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-792.342, -39.055, 686.614)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-798.100, -39.657, 628.171)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-911.372, -39.055, 628.077)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-912.799, -39.657, 589.312)
    }
]
ThreeToFourMoveAnimation.setKeys(ThreeToFourMoveKeyFrames);
ThreeToFourMoveAnimation.addEvent(event.thirdQuestion)

export const FourToFiveMoveAnimation = new Animation("cameraMoveFourToFive", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const FourToFiveMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-912.799, -39.657, 589.312)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-856.369, -39.657, 573.095)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-859.569, -39.055, 514.339)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-790.373, -39.657, 510.056)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-795.274, -39.055, 567.288)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-765.744, -39.657, 570.217)
    }
]
FourToFiveMoveAnimation.setKeys(FourToFiveMoveKeyFrames);
FourToFiveMoveAnimation.addEvent(event.fourthQuestion);

export const FiveToSixMoveAnimation = new Animation("cameraMoveFiveToSix", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const FiveToSixMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-765.744, -39.657, 570.217)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-677.626, -39.657, 499.527)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-679.721, -39.055, 454.087)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-562.311, -39.657, 455.608)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-560.552, -39.055, 424.761)
    }
]
FiveToSixMoveAnimation.setKeys(FiveToSixMoveKeyFrames);
FiveToSixMoveAnimation.addEvent(event.fifthQuestion);

export const SixToSevenMoveAnimation = new Animation("cameraMoveSixToSeven", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const SixToSevenMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-560.552, -39.657, 424.761)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-553.912, -39.657, 332.209)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-437.413, -39.055, 331.250)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-441.120, -39.657, 573.342)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-393.175, -39.055, 558.733)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-326.092, -39.657, 494.825)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-263.533, -39.055, 445.110)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-269.494, -39.657, 217.817)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-203.107, -39.055, 221.521)
    },
    {
        frame: 40*FRAME_RATE,
        value: new Vector3(-202.473, -39.657, 161.831)
    },
    {
        frame: 45*FRAME_RATE,
        value: new Vector3(-261.269, -39.657, 158.046)
    },
    {
        frame: 50*FRAME_RATE,
        value: new Vector3(-262.633, -39.055, 94.693)
    },
    {
        frame: 55*FRAME_RATE,
        value: new Vector3(-147.769, -39.657, 100.750)
    },
    {
        frame: 60*FRAME_RATE,
        value: new Vector3(-150.185, -39.055, -24.245)
    }
]
SixToSevenMoveAnimation.setKeys(SixToSevenMoveKeyFrames);
SixToSevenMoveAnimation.addEvent(event.sixthQuestion);

export const SevenToEightMoveAnimation = new Animation("cameraMoveSevenToEight", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const SevenToEightMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-150.185, -39.657, -24.245)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-259.299, -39.657, -22.654)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-261.433, -39.055, 36.266)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-328.262, -39.657, 38.147)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-324.004, -39.055, 99.621)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-378.439, -39.657, 101.793)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-377.553, -39.055, 156.238)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-324.361, -39.657, 157.881)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-324.312, -39.055, 219.866)
    },
    {
        frame: 40*FRAME_RATE,
        value: new Vector3(-362.642, -39.657, 218.341)
    }
]
SevenToEightMoveAnimation.setKeys(SevenToEightMoveKeyFrames);
SevenToEightMoveAnimation.addEvent(event.seventhQuestion);

export const EightToNineMoveAnimation = new Animation("cameraMoveEightToNine", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const EightToNineMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-362.642, -39.657, 218.341)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-385.368, -39.657, 224.597)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-382.433, -39.055, 220.167)
    },
    {
        frame: 8*FRAME_RATE,
        value: new Vector3(-387.368, -39.657, 282.597)
    },
    {
        frame: 11*FRAME_RATE,
        value: new Vector3(-620.386, -39.055, 272.969)
    },
    {
        frame: 14*FRAME_RATE,
        value: new Vector3(-616.612, -39.657, 331.756)
    },
    {
        frame: 17*FRAME_RATE,
        value: new Vector3(-680.006, -39.055, 331.087)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-681.766, -39.657, 278.110)
    },
    {
        frame: 23*FRAME_RATE,
        value: new Vector3(-801.237, -39.055, 275.647)
    },
    {
        frame: 26*FRAME_RATE,
        value: new Vector3(-798.719, -39.657, 210.918)
    },
    {
        frame: 29*FRAME_RATE,
        value: new Vector3(-854.706, -39.055, 210.967)
    },
    {
        frame: 32*FRAME_RATE,
        value: new Vector3(-857.230, -39.657, 159.426)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-973.735, -39.055, 154.336)
    },
    {
        frame: 38*FRAME_RATE,
        value: new Vector3(-975.060, -39.657, 95.983)
    },
    {
        frame: 41*FRAME_RATE,
        value: new Vector3(-1030.272, -39.055, 100.672)
    },
    {
        frame: 45*FRAME_RATE,
        value: new Vector3(-1029.912, -39.657, 37.302)
    },
    {
        frame: 49*FRAME_RATE,
        value: new Vector3(-1094.498, -39.055, 44.215)
    },
    {
        frame: 52*FRAME_RATE,
        value: new Vector3(-1097.722, -39.657, 160.748)
    },
    {
        frame: 55*FRAME_RATE,
        value: new Vector3(-1029.885, -39.055, 153.456)
    },
    {
        frame: 57*FRAME_RATE,
        value: new Vector3(-1034.022, -39.657, 275.796)
    },
    {
        frame: 60*FRAME_RATE,
        value: new Vector3(-1092.644, -39.055, 282.206)
    },
    {
        frame: 63*FRAME_RATE,
        value: new Vector3(-1087.660, -39.657, 392.754)
    },
    {
        frame: 66*FRAME_RATE,
        value: new Vector3(-1155.371, -39.055, 396.599)
    },
    {
        frame: 69*FRAME_RATE,
        value: new Vector3(-1155.974, -39.657, -24.092)
    },
    {
        frame: 72*FRAME_RATE,
        value: new Vector3(-1031.845, -39.055, -19.551)
    },
    {
        frame: 75*FRAME_RATE,
        value: new Vector3(-1036.431, -39.657, -75.288)
    },
    {
        frame: 78*FRAME_RATE,
        value: new Vector3(-970.243, -39.055, -78.962)
    },
    {
        frame: 81*FRAME_RATE,
        value: new Vector3(-973.682, -39.657, 37.613)
    },
    {
        frame: 84*FRAME_RATE,
        value: new Vector3(-918.372, -39.055, 29.031)
    },
    {
        frame: 87*FRAME_RATE,
        value: new Vector3(-915.185, -39.657, 95.254)
    },
    {
        frame: 90*FRAME_RATE,
        value: new Vector3(-856.358, -39.657, 94.057)
    }
]
EightToNineMoveAnimation.setKeys(EightToNineMoveKeyFrames);
EightToNineMoveAnimation.addEvent(event.eighthQuestion);

export const NineToTenMoveAnimation = new Animation("cameraMoveNineToTen", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const NineToTenMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-856.358, -39.657, 94.057)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-846.527, -39.657, -20.333)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-913.740, -39.055, -15.119)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-913.902, -39.657, -79.985)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-805.093, -39.055, -78.315)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-801.566, -39.657, -137.599)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-921.521, -39.055, -138.947)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-915.157, -39.657, -315.813)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-859.061, -39.055, -319.348)
    },
    {
        frame: 40*FRAME_RATE,
        value: new Vector3(-859.472, -39.657, -377.261)
    },
    {
        frame: 45*FRAME_RATE,
        value: new Vector3(-974.246, -39.055, -379.600)
    },
    {
        frame: 50*FRAME_RATE,
        value: new Vector3(-979.654, -39.657, -255.886)
    },
    {
        frame: 55*FRAME_RATE,
        value: new Vector3(-1091.643, -39.055, -258.225)
    },
    {
        frame: 60*FRAME_RATE,
        value: new Vector3(-1096.407, -39.657, -286.793)
    }
]
NineToTenMoveAnimation.setKeys(NineToTenMoveKeyFrames);
NineToTenMoveAnimation.addEvent(event.ninthQuestion);

export const TenToElevenMoveAnimation = new Animation("cameraMoveTenToEleven", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const TenToElevenMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-1096.407, -39.657, -286.793)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-1033.828, -39.657, -317.478)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-1033.784, -39.055, -376.434)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-1092.883, -39.657, -376.433)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-1096.499, -39.055, -491.554)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-1034.431, -39.657, -491.652)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-1032.440, -39.055, -434.154)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-975.005, -39.657, -437.534)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-973.544, -39.055, -493.578)
    },
    {
        frame: 40*FRAME_RATE,
        value: new Vector3(-916.040, -39.657, -494.869)
    },
    {
        frame: 45*FRAME_RATE,
        value: new Vector3(-913.262, -39.055, -549.395)
    },
    {
        frame: 50*FRAME_RATE,
        value: new Vector3(-852.834, -39.657, -551.142)
    },
    {
        frame: 55*FRAME_RATE,
        value: new Vector3(-849.853, -39.055, -492.182)
    },
    {
        frame: 60*FRAME_RATE,
        value: new Vector3(-674.244, -39.657, -495.756)
    },
    {
        frame: 65*FRAME_RATE,
        value: new Vector3(-674.136, -39.657, -374.622)
    },
    {
        frame: 70*FRAME_RATE,
        value: new Vector3(-563.280, -39.055, -375.918)
    },
    {
        frame: 75*FRAME_RATE,
        value: new Vector3(-563.387, -39.657, -492.588)
    },
    {
        frame: 80*FRAME_RATE,
        value: new Vector3(-622.848, -39.055, -492.471)
    },
    {
        frame: 85*FRAME_RATE,
        value: new Vector3(-618.081, -39.657, -670.837)
    },
    {
        frame: 90*FRAME_RATE,
        value: new Vector3(-560.710, -39.055, -672.801)
    },
    {
        frame: 95*FRAME_RATE,
        value: new Vector3(-560.90, -39.657, -735.787)
    },
    {
        frame: 100*FRAME_RATE,
        value: new Vector3(-622.767, -39.657, -732.767)
    },
    {
        frame: 105*FRAME_RATE,
        value: new Vector3(-618.039, -39.055, -851.321)
    },
    {
        frame: 110*FRAME_RATE,
        value: new Vector3(-556.016, -39.657, -851.217)
    },
    {
        frame: 115*FRAME_RATE,
        value: new Vector3(-553.846, -39.055, -786.264)
    },
    {
        frame: 120*FRAME_RATE,
        value: new Vector3(-442.566, -39.657, -792.444)
    },
    {
        frame: 125*FRAME_RATE,
        value: new Vector3(-442.568, -39.055, -908.585)
    },
    {
        frame: 130*FRAME_RATE,
        value: new Vector3(-502.771, -39.657, -909.146)
    },
    {
        frame: 135*FRAME_RATE,
        value: new Vector3(-498.999, -39.055, -996.353)
    }
]
TenToElevenMoveAnimation.setKeys(TenToElevenMoveKeyFrames);
TenToElevenMoveAnimation.addEvent(event.tenthQuestion);

export const ElevenToFinalMoveAnimation = new Animation("cameraMoveElevenToFinal", "position", FRAME_RATE, Animation.ANIMATIONTYPE_VECTOR3);
const ElevenToFinalMoveKeyFrames = [
    {
        frame: 0,
        value: new Vector3(-498.999, -39.657, -996.353)
    },
    {
        frame: 3*FRAME_RATE,
        value: new Vector3(-496.761, -39.657, -1021.035)
    },
    {
        frame: 5*FRAME_RATE,
        value: new Vector3(-439.327, -39.055, -1023.318)
    },
    {
        frame: 10*FRAME_RATE,
        value: new Vector3(-437.880, -39.657, -964.515)
    },
    {
        frame: 15*FRAME_RATE,
        value: new Vector3(-322.795, -39.055, -968.947)
    },
    {
        frame: 20*FRAME_RATE,
        value: new Vector3(-317.446, -39.657, -1085.448)
    },
    {
        frame: 25*FRAME_RATE,
        value: new Vector3(-260.748, -39.055, -1085.983)
    },
    {
        frame: 30*FRAME_RATE,
        value: new Vector3(-261.423, -39.657, -969.597)
    },
    {
        frame: 35*FRAME_RATE,
        value: new Vector3(-206.970, -39.055, -965.325)
    },
    {
        frame: 40*FRAME_RATE,
        value: new Vector3(-209.043, -39.657, -904.869)
    },
    {
        frame: 45*FRAME_RATE,
        value: new Vector3(-319.789, -39.055, -908.439)
    },
    {
        frame: 50*FRAME_RATE,
        value: new Vector3(-316.825, -39.657, -848.555)
    },
    {
        frame: 55*FRAME_RATE,
        value: new Vector3(-144.339, -39.055, -852.118)
    },
    {
        frame: 60*FRAME_RATE,
        value: new Vector3(-142.185, -39.657, -885.541)
    },
    {
        frame: 65*FRAME_RATE,
        value: new Vector3(31.733, -39.055, -1041.317)
    },
    {
        frame: 70*FRAME_RATE,
        value: new Vector3(31.763, -39.657, -1126.854)
    }
]
ElevenToFinalMoveAnimation.setKeys(ElevenToFinalMoveKeyFrames);
ElevenToFinalMoveAnimation.addEvent(event.finishJourney);