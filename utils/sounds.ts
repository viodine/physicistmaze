import { Sound } from '@babylonjs/core';
import { mainScene } from '..';

export const wrongAnswer = new Sound("wrongAnswer", "./assets/wrongAnswer.mp3", mainScene, null)

export const gong = new Sound("gong", "./assets/gong.wav", mainScene, null);
gong.setVolume(0.2);

export const bgMusic = new Sound("music", "./assets/bg.wav", mainScene, null, { loop: true });

export const intro = new Sound("intro", "./assets/intro.wav", mainScene, null);
export const question1 = new Sound("pracaSI", "./assets/questions/praca_si.wav", mainScene, null); // F
export const question2 = new Sound("zwierciadlo", "./assets/questions/zwierciadlo.wav", mainScene, null); // T
export const question3 = new Sound("hujgens", "./assets/questions/hujgens.wav", mainScene, null); // T
export const question4 = new Sound("dipol", "./assets/questions/dipol.wav", mainScene, null); // F
export const question5 = new Sound("ohm", "./assets/questions/ohm.wav", mainScene, null); // F
export const question6 = new Sound("absoluteZero", "./assets/questions/zero.wav", mainScene, null); // F
export const question7 = new Sound("momentSily", "./assets/questions/moment_sily.wav", mainScene, null); // F
export const question8 = new Sound("lorentz", "./assets/questions/lorentz.wav", mainScene, null); // F
export const question9 = new Sound("sem", "./assets/questions/sem.wav", mainScene, null); // T
export const question10 = new Sound("cewka", "./assets/questions/cewka.wav", mainScene, null); // T







