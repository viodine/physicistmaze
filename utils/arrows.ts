import { Vector2, Scene, PolygonMeshBuilder } from '@babylonjs/core';

export class Arrow {
    constructor(private scene: Scene, private answer: string) {}

    private corners = [ 
        new Vector2(0, 2),
        new Vector2(6, 2),
        new Vector2(6, 0),
        new Vector2(10, 5),
        new Vector2(6, 10),
        new Vector2(6, 8),
        new Vector2(0, 8)
  ];

    build(x: number, y: number, z: number, material) {
        const arrowBuilder = new PolygonMeshBuilder(`arrow${this.answer}`, this.corners, this.scene);
        let arrow = arrowBuilder.build(null, 0.5);
        arrow.position.x = x;
        arrow.position.y = y;
        arrow.position.z = z;
        arrow.material = material

        return arrow;
    }
}