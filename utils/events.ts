import { AnimationEvent, Vector3, MeshBuilder, StandardMaterial, DynamicTexture } from '@babylonjs/core';
import { FRAME_RATE, mainScene, light } from '../index'
import { Arrow } from './arrows';
import { Timer } from './timer';
import { gong, bgMusic, question1, question2, question3, question4, question5, question6, question7, question8, question9, question10 } from './sounds';
import { trueMaterial, falseMaterial } from './materials';

export const timer = new Timer();

export const startJourney = new AnimationEvent(5*FRAME_RATE, 
    () => {
        timer.start()
        gong.play();
        bgMusic.play();
        light.intensity = 0.5;
    },
    true
);

export const firstQuestion = new AnimationEvent(30*FRAME_RATE,
    () => {
        // Play question
        question1.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True1").build(-155.44, -32.99, 314.72, trueMaterial)
        leftArrow.rotate(new Vector3(0, 1, 0), Math.PI/2)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False1").build(-155.44, -32.99, 344.72, falseMaterial)
        rightArrow.rotate(new Vector3(0, 1, 0), -Math.PI/2)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
    },
    true
);

export const secondQuestion = new AnimationEvent(77*FRAME_RATE,
    () => {
        // Play question
        question2.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True2").build(-704.47, -32.99, 758.45, trueMaterial);
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2);
        leftArrow.rotate(new Vector3(0, 1, 0), Math.PI);
        const forwardArrow = new Arrow(mainScene, "False2").build(-659.591, -32.99, 746.48, falseMaterial);
        forwardArrow.rotate(new Vector3(1, 0, 0), Math.PI/2);
        forwardArrow.rotate(new Vector3(0, 0, 1), Math.PI/2);
    },
    true
);

export const thirdQuestion = new AnimationEvent(30*FRAME_RATE,
    () => {
        // Play question
        question3.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True3").build(-914.45, -32.99, 563.70, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False3").build(-932.59, -32.99, 563.70, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 1, 0), Math.PI)
    },
    true
);

export const fourthQuestion = new AnimationEvent(20*FRAME_RATE,
    () => {
        // Play question
        question4.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True4").build(-687.7, -32.99, 574.18, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        leftArrow.rotate(new Vector3(0, 0, 1), Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False4").build(-741.42, -32.99, 543.42, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 1, 0), Math.PI)
        rightArrow.rotate(new Vector3(0, 0, 1), -Math.PI/2)
    }
);

export const fifthQuestion = new AnimationEvent(15*FRAME_RATE,
    () => {
        // Play question
        question5.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True5").build(-538, -32.99, 401.82, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        const farLeftArrow = new Arrow(mainScene, "False5").build(-570.70, -32.99, 328.17, falseMaterial)
        farLeftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
    },
    true
);

export const sixthQuestion = new AnimationEvent(60*FRAME_RATE,
    () => {
        // Play question
        question6.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True6").build(-200.48, -32.99, -36.96, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        leftArrow.rotate(new Vector3(0, 0, 1), -Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False6").build(-225.31, -32.99, -19.75, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 0, 1), Math.PI/2)
    },
    true
);

export const seventhQuestion = new AnimationEvent(40*FRAME_RATE,
    () => {
        // Play question
        question7.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True7").build(-407.27, -32.99, 216.88, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        leftArrow.rotate(new Vector3(0, 0, 1), -Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False7").build(-417.40, -32.99, 279.99, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 1, 0), Math.PI)
    },
    true
);

export const eighthQuestion = new AnimationEvent(90*FRAME_RATE,
    () => {
        // Play question
        question8.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True8").build(-822, -32.99, 30.7, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False8").build(-859.41, -32.99, 36.27, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 1, 0), Math.PI)
    },
    true
);

export const ninthQuestion = new AnimationEvent(60*FRAME_RATE,
    () => {
        // Play question
        question9.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True9").build(-1089.58, -32.99, -313.95, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False9").build(-1111.58, -32.99, -313.95, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 1, 0), Math.PI)
    },
    true
);

export const tenthQuestion = new AnimationEvent(135*FRAME_RATE,
    () => {
        // Play question
        question10.play();
        // Show arrows
        const leftArrow = new Arrow(mainScene, "True10").build(-504.56, -32.99, -1030, trueMaterial)
        leftArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        const rightArrow = new Arrow(mainScene, "False10").build(-504.56, -32.99, -1031.3, falseMaterial)
        rightArrow.rotate(new Vector3(1, 0, 0), Math.PI/2)
        rightArrow.rotate(new Vector3(0, 1, 0), Math.PI)
    },
    true
);

export const finishJourney = new AnimationEvent(70*FRAME_RATE,
    () => {
        timer.stop()
        const results = MeshBuilder.CreatePlane("event12", {height: 50, width: 30}, mainScene);
        results.position = new Vector3(34.08, -27.99, -1118.93) 

    //Set width and height for dynamic texture using same multiplier
    const DTWidth = 80 * 60;
    const DTHeight = 50 * 60;
    //Set text
    let text = `Twój czas to \n${timer.time} sekund`;
    //Create dynamic texture
    const dynamicTexture = new DynamicTexture("ResultsTexture", {width:DTWidth, height:DTHeight}, mainScene, false);

    //Check width of text for given font type at any size of font
    const ctx = dynamicTexture.getContext();
	const size = 48; //any value will work
    ctx.font = size + "px " + "Arial";
    const textWidth = ctx.measureText(text).width;
    
    //Calculate ratio of text width to size of font used
    const ratio = textWidth/size;
	
	//set font to be actually used to write text on dynamic texture
    const font_size = Math.floor(DTWidth / (ratio * 1)); //size of multiplier (1) can be adjusted, increase for smaller text
    const font = font_size + "px " + "Arial";
	
	//Draw text
    dynamicTexture.drawText(text, null, null, font, "#000000", "#ffffff", true);

    //create material
    var resultsMat = new StandardMaterial("resultsMat", mainScene);
    resultsMat.diffuseTexture = dynamicTexture;
    
    //apply material
    results.material = resultsMat;
    }
);