import { Engine, SceneLoader, Scene, Vector3,
    HemisphericLight, MeshBuilder, StandardMaterial, Texture,
    CubeTexture, Color3 } from '@babylonjs/core';
import { HolographicButton } from '@babylonjs/gui';

const canvas: any = document.getElementById("renderCanvas");
const engine: Engine = new Engine(canvas, true);
export const mainScene: Scene = new Scene(engine);
export const FRAME_RATE = 60;
export const light = new HemisphericLight("HemiLight", new Vector3(0, 1, 0), mainScene);

import { timer } from './utils/events';
import { trueMaterial, falseMaterial } from './utils/materials';
import { wrongAnswer, intro } from './utils/sounds'
import { introMoveAnimation, OneToTwoMoveAnimation, TwoToThreeMoveAnimation, ThreeToFourMoveAnimation,
        FourToFiveMoveAnimation, FiveToSixMoveAnimation, SixToSevenMoveAnimation, SevenToEightMoveAnimation,
        EightToNineMoveAnimation, NineToTenMoveAnimation, TenToElevenMoveAnimation, ElevenToFinalMoveAnimation } from './utils/animations'

// Load Maze form .babylon file
SceneLoader.Append("", "fizbit_maze3.babylon", mainScene, (newScene: Scene) => {
    // Main settings
    const scene = newScene;
    scene.gravity = new Vector3(0, -0.9, 0);

    //VR Helper settings
    const VRHelper = scene.createDefaultVRExperience();
    VRHelper.enableInteractions();
    let selectingArrow = false;
    let selectedArrow = false;
    let selectedIndex: number;

    scene.registerBeforeRender(() => {
        for (var i = 0; i < scene.meshes.filter((mesh) => mesh.name.includes("arrow")).length; i++) {
        if (selectingArrow && !selectedArrow) {
            scene.meshes[selectedIndex].scaling.x += 0.001;
            scene.meshes[selectedIndex].scaling.y += 0.001;
            scene.meshes[selectedIndex].scaling.z += 0.001;

            if (scene.meshes[selectedIndex].scaling.x >= 1.2) {
                selectedArrow = true;
            }
        }
        if (selectedArrow) {
            const selectedMaterial = new StandardMaterial("selected", scene)
            selectedMaterial.diffuseColor = new Color3(127, 0, 230);
            scene.meshes[selectedIndex].material = selectedMaterial
        }
    }
    });

    VRHelper.raySelectionPredicate = (mesh) => {
        if (mesh.name.includes("arrow")) {
            return true;
        }
        return false;
    };

    VRHelper.onNewMeshSelected.add((mesh) => {
        if (mesh.name.includes("arrow")) {
            selectedIndex = scene.meshes.indexOf(mesh)
            const selectingMaterial = new StandardMaterial("selecting", scene);
            selectingMaterial.diffuseColor = new Color3(0, 70, 129);
            mesh.material = selectingMaterial
            selectingArrow = true;
        }  
    });

    VRHelper.onSelectedMeshUnselected.add((selectedMesh) => {
        selectingArrow = false;
        selectedArrow = false;
        if (selectedIndex !== -1) {
            selectedMesh.material = selectedMesh.name.includes("True") ? trueMaterial : falseMaterial;
            selectedMesh.scaling.x = 1;
            selectedMesh.scaling.y = 1;
            selectedMesh.scaling.z = 1;
            selectedIndex = -1;
        }
    })

    // Camera Settings
    VRHelper.deviceOrientationCamera.speed = 2.8;
    VRHelper.deviceOrientationCamera.ellipsoid = new Vector3(0.5, 5, 0.5);
    VRHelper.deviceOrientationCamera.position = new Vector3(-608.773, 501.862, -271.969);

    VRHelper.vrDeviceOrientationCamera.speed = 1.8;
    VRHelper.vrDeviceOrientationCamera.position = new Vector3(-608.773, 501.862, -271.969);
    VRHelper.vrDeviceOrientationCamera.ellipsoid = new Vector3(0.5, 5, 0.5);
    
    // Floor setup
    const ground = MeshBuilder.CreateGround("ground", {height: 3000, width: 3000, subdivisions: 4}, scene);
    const groundMaterial = new StandardMaterial("groundMaterial", scene);
    groundMaterial.diffuseTexture = new Texture("./textures/ground.jpg", scene);
    (<Texture>groundMaterial.diffuseTexture).uScale = 50.0;
    (<Texture>groundMaterial.diffuseTexture).vScale = 50.0;
    ground.position.y = -50;
    ground.material = groundMaterial;

    // Skybox settings
    const skybox = MeshBuilder.CreateBox("skyBox", {size:10000.0}, scene);
	const skyboxMaterial = new  StandardMaterial("skyBox", scene);
	skyboxMaterial.backFaceCulling = false;
	skyboxMaterial.reflectionTexture = new  CubeTexture("textures/skybox/elbrus", scene);
	skyboxMaterial.reflectionTexture.coordinatesMode =  Texture.SKYBOX_MODE;
	skyboxMaterial.diffuseColor = new  Color3(0, 0, 0);
	skyboxMaterial.specularColor = new  Color3(0, 0, 0);
	skybox.material = skyboxMaterial;	

    // Dev wireframe
    // var matBB = new StandardMaterial("matBB", scene);
    // matBB.emissiveColor = new Color3(1, 1, 1);
    // matBB.wireframe = true;
    // scene.meshes[2].material = matBB;		
	    
    scene.executeWhenReady(() => {
        scene.onPointerDown = () => {
            // Play intro
            intro.play();
            scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [introMoveAnimation], 0, 10*FRAME_RATE, false, undefined);
            intro.onEndedObservable.add(() => {
                scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [OneToTwoMoveAnimation], 0, 30*FRAME_RATE, false, undefined, () => {
                    scene.onPointerDown = () => {
                        if (scene.meshes[selectedIndex].name === "arrowFalse1" && selectedArrow === true) {
                            scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [TwoToThreeMoveAnimation], 0, 77*FRAME_RATE, false, undefined, () => {
                                scene.onPointerDown = () => {
                                    if (scene.meshes[selectedIndex].name === "arrowTrue2" && selectedArrow === true) {
                                        scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [ThreeToFourMoveAnimation], 0, 30*FRAME_RATE, false, undefined, () => {
                                            scene.onPointerDown = () => {
                                                if (scene.meshes[selectedIndex].name === "arrowTrue3" && selectedArrow === true) {
                                                    scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [FourToFiveMoveAnimation], 0, 20*FRAME_RATE, false, undefined, () => {
                                                        scene.onPointerDown = () => {
                                                            if (scene.meshes[selectedIndex].name === "arrowFalse4" && selectedArrow === true) {
                                                                scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [FiveToSixMoveAnimation], 0, 15*FRAME_RATE, false, undefined, () => {
                                                                    scene.onPointerDown = () => {
                                                                        if (scene.meshes[selectedIndex].name === "arrowFalse5" && selectedArrow === true) {
                                                                            scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [SixToSevenMoveAnimation], 0, 60*FRAME_RATE, false, undefined, () => {
                                                                                scene.onPointerDown = () => {
                                                                                    if (scene.meshes[selectedIndex].name === "arrowFalse6" && selectedArrow === true) {
                                                                                        scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [SevenToEightMoveAnimation], 0, 40*FRAME_RATE, false, undefined, () => {
                                                                                            scene.onPointerDown = () => {
                                                                                                if (scene.meshes[selectedIndex].name === "arrowFalse7" && selectedArrow === true) {
                                                                                                    scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [EightToNineMoveAnimation], 0, 90*FRAME_RATE, false, undefined, () => {
                                                                                                        scene.onPointerDown = () => {
                                                                                                            if (scene.meshes[selectedIndex].name === "arrowFalse8" && selectedArrow === true) {
                                                                                                                scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [NineToTenMoveAnimation], 0, 60*FRAME_RATE, false, undefined, () => {
                                                                                                                    scene.onPointerDown = () => {
                                                                                                                        if (scene.meshes[selectedIndex].name === "arrowTrue9" && selectedArrow === true) {
                                                                                                                            scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [TenToElevenMoveAnimation], 0, 135*FRAME_RATE, false, undefined, () => {
                                                                                                                                scene.onPointerDown = () => {
                                                                                                                                    if (scene.meshes[selectedIndex].name === "arrowTrue10" && selectedArrow === true) {
                                                                                                                                        scene.beginDirectAnimation(VRHelper.vrDeviceOrientationCamera, [ElevenToFinalMoveAnimation], 0, 70*FRAME_RATE, false, undefined)
                                                                                                                                    } else if (scene.meshes[selectedIndex].name === "arrowFalse10" && selectedArrow === true) {
                                                                                                                                        // Wrong answer
                                                                                                                                        wrongAnswer.play();
                                                                                                                                        timer.penalty();
                                                                                                                                    } else {
                                                                                                                                        // U need to answer
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            })
                                                                                                                        } else if (scene.meshes[selectedIndex].name === "arrowFalse9" && selectedArrow === true) {
                                                                                                                            // Wrong answer
                                                                                                                            wrongAnswer.play();
                                                                                                                            timer.penalty();
                                                                                                                        } else {
                                                                                                                            // U need to answer
                                                                                                                        }
                                                                                                                    }
                                                                                                                })
                                                                                                            } else if (scene.meshes[selectedIndex].name === "arrowTrue8" && selectedArrow === true) {
                                                                                                                // Wrong answer
                                                                                                                wrongAnswer.play();
                                                                                                                timer.penalty();
                                                                                                            } else {
                                                                                                                // U need to answer
                                                                                                            }
                                                                                                        }
                                                                                                    })
                                                                                                } else if (scene.meshes[selectedIndex].name === "arrowTrue7" && selectedArrow === true) {
                                                                                                    // Wrong answer
                                                                                                    wrongAnswer.play();
                                                                                                    timer.penalty();
                                                                                                } else {
                                                                                                    // U need to answer
                                                                                                }
                                                                                            }
                                                                                        })
                                                                                    } else if (scene.meshes[selectedIndex].name === "arrowTrue6" && selectedArrow === true) {
                                                                                        // Wrong answer
                                                                                        wrongAnswer.play();
                                                                                        timer.penalty();
                                                                                    } else {
                                                                                        // U need to answer
                                                                                    }
                                                                                }
                                                                            })
                                                                        } else if (scene.meshes[selectedIndex].name === "arrowTrue5" && selectedArrow === true) {
                                                                            // Wrong answer
                                                                            wrongAnswer.play();
                                                                            timer.penalty();
                                                                        } else {
                                                                            // U need to answer
                                                                        }
                                                                    }
                                                                })
                                                            } else if (scene.meshes[selectedIndex].name === "arrowTrue4" && selectedArrow === true) {
                                                                // Wrong answer
                                                                wrongAnswer.play();
                                                                timer.penalty();
                                                            } else {
                                                                // U need to answer
                                                            }
                                                        }
                                                    })
                                                } else if (scene.meshes[selectedIndex].name === "arrowFalse3" && selectedArrow === true) {
                                                    // Wrong answer
                                                    wrongAnswer.play();
                                                    timer.penalty();
                                                } else {
                                                    // U need to answer
                                                }
                                            }
                                        })
                                    } else if (scene.meshes[selectedIndex].name === "arrowFalse2" && selectedArrow === true) {
                                        // Wrong answer
                                        wrongAnswer.play();
                                        timer.penalty();
                                    } else {
                                        // U need to answer
                                    }
                            }
                        })} else if (scene.meshes[selectedIndex].name === "arrowTrue1" && selectedArrow === true) {
                            // Wrong answer
                            wrongAnswer.play();
                            timer.penalty();
                        } else {
                            // U need to answer
                        }
                    }
            });
            })   
        };
        engine.runRenderLoop(() => {
            // console.log(`${VRHelper.deviceOrientationCamera.position.x.toFixed(3)}X | ${VRHelper.deviceOrientationCamera.position.y.toFixed(3)}Y |${VRHelper.deviceOrientationCamera.position.z.toFixed(3)}Z |${VRHelper.deviceOrientationCamera.rotation.x.toFixed(3)}X | ${VRHelper.deviceOrientationCamera.rotation.y.toFixed(3)}Y | ${VRHelper.deviceOrientationCamera.rotation.z.toFixed(3)}Z`);
            // document.getElementById("fps").innerText = `${engine.getFps().toFixed()} FPS || Timer: ${timer.time.toString()} || ${scene.meshes}`;
            // document.getElementById("camPosition").innerText = `${VRHelper.deviceOrientationCamera.position.x.toFixed(3)}X | ${VRHelper.deviceOrientationCamera.position.y.toFixed(3)}Y |${VRHelper.deviceOrientationCamera.position.z.toFixed(3)}Z`;
            scene.render();
        })
    })
    return scene;
})