const path = require("path");

module.exports = {
    entry: './index.ts',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: path.resolve(__dirname, 'dist')
    },
    devServer: {
        index: "./dist/index.html",
        disableHostCheck: true, 
        open: true
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    externals: {
        "oimo": true,
        "cannon": true,
        "earcut": true
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    },
    mode: "development"
};