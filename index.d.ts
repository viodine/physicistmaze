import { Scene, HemisphericLight } from '@babylonjs/core';
export declare const mainScene: Scene;
export declare const FRAME_RATE = 60;
export declare const light: HemisphericLight;
